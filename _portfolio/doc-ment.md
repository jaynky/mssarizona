---
layout: post
title: Doc-Ment
description: A personal documentation library.  
image: /images/doc-ment.png 
link: https://nostalgic-albattani-b8513a.netlify.com/
gitlab: https://gitlab.com/jaynky/doc-ment
---

<a href="https://nostalgic-albattani-b8513a.netlify.com/"><img src="/images/doc-ment.png"></a>

Doc-Ment was a personal project to keep track of what we learned in order
to reduce time looking up how to code an element. It was maintained for a few weeks
but ultimately died as the time it took to update was too much, and there were little 
benefits.
